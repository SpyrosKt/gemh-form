A platform intended to enable employees of the General Commercial Registry of Greece to access Articles of Association of SA companies (in the form of .pdf files) in order to extract and register information through an easily manageable form connected to a database.

The platform is currently hosted at https://gemh.000webhostapp.com/